// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.5/firebase-app.js";
import { getAuth, signInWithPopup, GoogleAuthProvider, signOut } from "https://www.gstatic.com/firebasejs/9.6.5/firebase-auth.js";
import { getDatabase, ref, set, get, child, remove } from "https://www.gstatic.com/firebasejs/9.6.5/firebase-database.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyD6I8FIWb415EumZhvAauzQPGtWTKz1lzU",
    authDomain: "clouds5-weiher.firebaseapp.com",
    databaseURL: "https://clouds5-weiher-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "clouds5-weiher",
    storageBucket: "clouds5-weiher.appspot.com",
    messagingSenderId: "350711344816",
    appId: "1:350711344816:web:092de3c1e28f35d4c5d575"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const provider = new GoogleAuthProvider();
const auth = getAuth();
const database = getDatabase(app);
var showFavourites = false

var current_username = null
var currentData = null

var start_row = new Vue({
    el: '#start_row',
    data: {
        hidden: false
    },
    methods: {
        googleLogin: function () {
            console.log("SignIn")
            signInWithPopup(auth, provider)
                .then((result) => {
                    // This gives you a Google Access Token. You can use it to access the Google API.
                    const credential = GoogleAuthProvider.credentialFromResult(result);
                    const token = credential.accessToken;
                    // The signed-in user info.
                    const user = result.user;
                    current_username = user["displayName"]
                    navbar.user = current_username
                    navbar.hidden = false
                    start_row.hidden = true
                    myTable.hidden = false
                    console.log("Successfull Login")

                    // ...
                }).catch((error) => {
                    // Handle Errors here.
                    const errorCode = error.code;
                    const errorMessage = error.message;
                    // The email of the user's account used.
                    const email = error.email;
                    // The AuthCredential type that was used.
                    const credential = GoogleAuthProvider.credentialFromError(error);
                    console.log("Failed Login")
                    // ...
                });

        }
    }
})


function writeAllMovies(Id, title, year, genre, path = 'movies-list/') {
    const db = getDatabase();
    set(ref(db, path + Id), {
        title: title,
        year: year,
        genre: genre
    });
}


function deleteAllMovies(complete_path) {
    const db = getDatabase();
    console.log("remove: " + complete_path)
    set(ref(db, complete_path), {
        title: null,
        year: null,
        genre: null
    });
    //remove(ref(db, current_username + Id))
}
//writeAllMovies()


//Second Page
var navbar = new Vue({
    el: '.navbar',
    data: {
        user: "",
        hidden: true
    },
    methods: {
        logout: function () {
            const auth = getAuth();
            signOut(auth).then(() => {
                // Sign-out successful.
                navbar.user = ""
                navbar.hidden = true
                start_row.hidden = false
                myTable.hidden = true
                myTableUser.hidden = true
                removeRows("actual_table")
                removeRows("actual_table_user")
                getMovies("no_path", "actual_table")
            }).catch((error) => {
                // An error happened.
                console.log("Logout failed")
            });

        },
        favourites: function () {
            if (showFavourites) {
                myTableUser.hidden = true
                myTable.hidden = false
                removeRows("actual_table_user")

            } else {
                myTable.hidden = true
                getMovies(current_username, "actual_table_user")
                myTableUser.hidden = false
                removeRows("actual_table")
                getMovies("no_path", "actual_table")
            }

            showFavourites = !showFavourites
        }
    }
})


var myTable = new Vue({
    el: '#myTable',
    data: {
        hidden: true
    },
    methods: {
        filterTable: function () {
            console.log("lets Filter")
            filterTable("myTable", "myInput")
        }
    }
})


var myTableUser = new Vue({
    el: '#myTableUser',
    data: {
        hidden: true
    },
    methods: {
        filterTable: function () {
            console.log("lets Filter")
            filterTable()
        }
    }
})


async function fetchJson() {
    const response = await fetch("movies.json")
    const data = await response.json()
    const movie_array = data["movies-list"]
    for (const element of movie_array) {
        writeAllMovies(element["id"], element["title"], element["year"], element["genre"])
    }
}
//fetchJson()


function getMovies(path, table_name) {
    const dbRef = ref(getDatabase());
    //console.log(get(child(dbRef, `books`)))
    console.log(path, table_name)
    if (path == "no_path") {
        rowUpdate(table_name, currentData)
        return
    }
    get(child(dbRef, path)).then((snapshot) => {
        if (snapshot.exists()) {
            var data = snapshot.val()
            if (table_name == "actual_table") {
                currentData = data
            }
            rowUpdate(table_name, data)
        } else {
            console.log("No data available");
        }
    }).catch((error) => {
        console.error(error);
    });
}
getMovies(`movies-list`, "actual_table")






function filterTable(table, input) {
    // Declare variables
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(input);
    filter = input.value.toUpperCase();
    table = document.getElementById(table);
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
function like(Id) {
    document.getElementById(Id + "actual_table").style.display = "none";
    writeAllMovies(Id, currentData[Id]["title"], currentData[Id]["year"], currentData[Id]["genre"], (current_username + '/'))
}

function dislike(Id) {
    document.getElementById(Id + "actual_table_user").style.display = "none";
    deleteAllMovies((current_username + '/' + Id))
}

function removeRows(table_name) {
    const elements = document.getElementsByClassName(table_name + "deleteRow");
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}


function rowUpdate(table_name, data) {
    for (const [key, value] of Object.entries(data)) {

        var table = document.getElementById(table_name);
        var row = table.insertRow(1);
        row.classList.add(table_name + "deleteRow")
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4)

        cell1.innerHTML = key;
        cell2.innerHTML = value["title"];
        cell3.innerHTML = value["year"];
        cell4.innerHTML = value["genre"];
        cell5.innerHTML = `<button type="button" class="btn btn-light like_button" id="${key}${table_name}"> <img src="https://img.icons8.com/material-rounded/24/000000/like--v1.png"/></button>`

        if (table_name == "actual_table") {
            function myFunction() {
                like(key)
            }
            document.getElementById(key + table_name).addEventListener("click", myFunction)
        } else if (table_name == "actual_table_user") {
            function myFunction() {
                dislike(key)
            }
            document.getElementById(key + table_name).addEventListener("click", myFunction)
        }

    }
}
